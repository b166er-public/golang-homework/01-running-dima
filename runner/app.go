package main

import "fmt"

// Directions
//        N
//        |
//        |
//  W ----o---- E
//        |
//        |
//        S

type Direction int

const (
	DIRECTION_NORTH Direction = 0
	DIRECTION_EAST  Direction = 1
	DIRECTION_SOUTH Direction = 2
	DIRECTION_WEST  Direction = 3
)

func (d Direction) turnClockWise() Direction {
	switch d {
	case DIRECTION_NORTH:
		return DIRECTION_EAST
	case DIRECTION_EAST:
		return DIRECTION_SOUTH
	case DIRECTION_SOUTH:
		return DIRECTION_WEST
	case DIRECTION_WEST:
		return DIRECTION_NORTH
	default:
		// Return old value if the direction is uknown
		return d
	}
}

func (d Direction) turnCounterClockWise() Direction {
	switch d {
	case DIRECTION_NORTH:
		return DIRECTION_WEST
	case DIRECTION_EAST:
		return DIRECTION_NORTH
	case DIRECTION_SOUTH:
		return DIRECTION_EAST
	case DIRECTION_WEST:
		return DIRECTION_SOUTH
	default:
		// Return old value if the direction is uknown
		return d
	}
}

type Runner struct {
	// Hint : We need to keep track where the runner is currently located
	x int
	y int

	// Hint : Initial direction of the runner is to the NORTH
	direction Direction

	// Hint : We need to know how big is the overall distance the runner already completed
	overallDistance int
}

// The runner does not run, but change the orientation by 90 degree clockwise (e.g. from NORTH to EAST)
func (r *Runner) turnClockWise() {
	if r.direction != 3 {
		r.direction += 1
		// fmt.Println(r.direction)

	} else {
		r.direction = 0
		// fmt.Println(r.direction)

	}
}

// The runner does not run, but change the orientation by 90 degree counter clockwise (e.g. from EAST to NORTH)
func (r *Runner) turnCounterClockWise() {
	if r.direction != 0 {
		r.direction -= 1
		// fmt.Println(r.direction)

	} else {
		r.direction = 3
		// fmt.Println(r.direction)

	}
}

// The method "run" let the runner run for some meters into the direction it is currently looking
// Hint : X axis shows to the EAST, Y axis shows to the NORTH
// Hint : Moving to NORTH -> Y-Position gets higher
// Hint : Moving to EAST -> X-Position gets higher
// Hint : Moving to SOUTH -> Y-Position gets lower
// Hint : Moving to WEST -> X-Position gets lower
func (r *Runner) run(distance int) {
	if r.direction == 0 {
		r.y += distance
		r.overallDistance += distance

	} else if r.direction == 1 {
		r.x += distance
		r.overallDistance += distance

	} else if r.direction == 2 {
		r.y -= distance
		r.overallDistance += distance

	} else if r.direction == 3 {
		r.x -= distance
		r.overallDistance += distance

	} else {
		fmt.Println("Something wrong...")

	}
}

// Tell us how many meters the runner ran at all during the trip
func (r *Runner) tripDistance() int {
	return r.overallDistance
}

// Tell us where on X axis the runner is currently standing
func (r *Runner) getPositionX() int {
	return r.x
}

// Tell us where on Y axis the runner is currently standing
func (r *Runner) getPositionY() int {
	return r.y
}

// Tell us in witch direction the runner is currently looking
func (r *Runner) getDirection() Direction {
	if r.direction == 0 {
		return DIRECTION_NORTH
	} else if r.direction == 1 {
		return DIRECTION_EAST
	} else if r.direction == 2 {
		return DIRECTION_SOUTH
	} else if r.direction == 3 {
		return DIRECTION_WEST
	} else {
		fmt.Printf("\nSomething wrong...")
		return 0
	}
	
}








// ------------------------------------------------------------------------------------------------------------------------//
/**
* From here the tests begin
* ATTENTION : From this line on no changes are allowed.
 */

func dima_trip_1() bool {
	var dima Runner

	// Dima beginns his walk looking towards NORTH
	if dima.getDirection() != DIRECTION_NORTH {
		return false
	}

	// Let us run 100 meter
	dima.run(100)

	// After we ran 100 meter our distance should be exactly 100 meter
	if dima.tripDistance() != 100 {
		return false
	}

	// Now we will turn two times into clockwise direction (180 degree) and run again 100 meter
	dima.turnClockWise()
	dima.turnClockWise()
	dima.run(100)

	// We expect to be now back at the origin (0,0)
	if (dima.getPositionX() != 0) || (dima.getPositionY() != 0) {
		return false
	}

	// Dima continue running 50 meter in the same direction
	dima.run(50)

	// We expect Dima looking into the sounth direction
	if dima.getDirection() != DIRECTION_SOUTH {
		return false
	}

	// Dima now should be located at (0,-50)
	if (dima.getPositionX() != 0) || (dima.getPositionY() != -50) {
		return false
	}

	// Dima now stops his trip and checks how long he was running ... the distance is 250 meter
	if dima.tripDistance() != 250 {
		return false
	}

	return true
}

func dima_trip_2() bool {
	var dima Runner

	// Dima runs 50 meter to NORTH
	dima.run(50)

	// ... then he turns 90 degree counter clockwise
	dima.turnCounterClockWise()

	// ... then he run again 50 meter
	dima.run(50)

	// ... then he turns again 90 degree counter clockwise
	dima.turnCounterClockWise()

	// ... then he run again 50 meter
	dima.run(50)

	// ... then Dima last time turns 90 degree counter clockwise
	dima.turnCounterClockWise()

	// ... and finally run again 50 meter
	dima.run(50)

	// Now Dima should have run 200 meter
	if dima.tripDistance() != 200 {
		return false
	}

	// ... and be again back in the origin
	if (dima.getPositionX() != 0) || (dima.getPositionY() != 0) {
		return false
	}

	// ... and looking to EAST
	if dima.getDirection() != DIRECTION_EAST {
		return false
	}

	return true
}

func dima_trip_3() bool {
	var dima Runner

	// Dima wants to start running to SOUTH
	dima.turnClockWise()
	dima.turnClockWise()
	if dima.getDirection() != DIRECTION_SOUTH {
		return false
	}

	// Dima run 100 meter and should be at (0,-100)
	dima.run(100)
	if (dima.getPositionX() != 0) || (dima.getPositionY() != -100) {
		return false
	}

	// Now Dima turns to the WEST and run 500 meter ... should be now at (-500, -100)
	dima.turnClockWise()
	dima.run(500)
	if (dima.getPositionX() != -500) || (dima.getPositionY() != -100) {
		return false
	}

	// Dima decided to stay for a while at the new location and check how much he had run ... 600 meter
	if dima.tripDistance() != 600 {
		return false
	}

	return true
}

func main() {
	if dima_trip_1() {
		fmt.Println("Trip 1 ... OK!")
	} else {
		fmt.Println("Trip 1 ... FAILED!")
	}

	if dima_trip_2() {
		fmt.Println("Trip 2 ... OK!")
	} else {
		fmt.Println("Trip 2 ... FAILED!")
	}

	if dima_trip_3() {
		fmt.Println("Trip 3 ... OK!")
	} else {
		fmt.Println("Trip 3 ... FAILED!")
	}
}
