# 01-running-dima

This is a homework which should help learning the programming language Go. In this homework we will focus on following topics:

- What is a struct?
- What is a method?
- How to build an application?
- How to run an application?
- How to debug an application?

Let's start!

# Folder ```vector```

In this folder all the concepts of the homework are used to show how in Go one could program a 3D vector.

## Build

To build the vector application change into the ```vector``` directory and execute ...

```bash
go build app.go
```

If everything was ok, a new file in the ```vector``` folder will appear which is the named either ```app``` or ```app.exe```

## Run

To run the vector application change into the ```vector``` directory and execute ...

```bash
./app
```

The output should look like this ...

```
Vector v1 =  {1 2 3}
Vector v2 =  {-2 5 8}
Vector v1 + v2 =  {-1 7 11}
Length of v1 =  3.7416573867739413
Inverted vector v1 =  {-1 -2 -3}
v1 + v1' = 0
v2 length :  9.643650760992955
v2 scaled with 3 length :  28.930952282978865
9.643650760992955  * 3.0 =  28.930952282978865
```

## Debug

To debug the application simply go to the Debugger and select ```Debug vector```

# Folder ```runner```

In this folder the student should use the same concepts like in ```vector``` to implement a virtual runner which run on two dimensional plane.

## Build

To build the vector application change into the ```runner``` directory and execute ...

```bash
go build app.go
```

If everything was ok, a new file in the ```runner``` folder will appear which is the named either ```app``` or ```app.exe```

## Run

To run the runner application change into the ```runner``` directory and execute ...

```bash
./app
```

The output of a completed homework should look like this ...

```
Trip 1 ... OK!
Trip 2 ... OK!
Trip 3 ... OK!
Trip 4 ... OK!
```

## Debug

To debug the application simply go to the Debugger and select ```Debug runner```

