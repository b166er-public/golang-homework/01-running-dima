package main

import (
	"fmt"
	"math"
)

/*
* Here we describe the structure of a 3D vector.
* This (the structure) answers the question - What is inside a 3D vector?
 */
type Vector3D struct {
	x float64
	y float64
	z float64
}

/*
* Here we describe the behaviour of a 3D vector
* The methods define the behaviour and answers the question - What you can do with a vector?
 */

// The method "abs" tell us the length of a vector by using euclidian norm - sqrt(x^2 + y^2 + z^2)
func (v *Vector3D) abs() float64 {
	var result float64 = math.Sqrt((v.x * v.x) + (v.y * v.y) + (v.z * v.z))
	return result
}

// The method "scale" will multiply all dimensions of a vector by a factor
func (v *Vector3D) scale(factor float64) {
	v.x = v.x * factor
	v.y = v.y * factor
	v.z = v.z * factor
}

// The method "invert" will create a vector v' for vecto v so that v + v' = 0
func (v *Vector3D) invert() {
	v.scale(-1.0)
}

func (v *Vector3D) copy() Vector3D {
	return Vector3D{v.x, v.y, v.z}
}

// The function "sum" takes two 3D vectors and return the sum of both vectors
func Sum(v1 *Vector3D, v2 *Vector3D) Vector3D {
	var result Vector3D = Vector3D{0.0, 0.0, 0.0}
	result.x = v1.x + v2.x
	result.y = v1.y + v2.y
	result.z = v1.z + v2.z
	return result
}

// This is the entry point for the vector application
func main() {
	var v1 Vector3D = Vector3D{1.0, 2.0, 3.0}
	var v2 Vector3D = Vector3D{-2.0, 5.0, 8.0}

	fmt.Println("Vector v1 = ", v1)
	fmt.Println("Vector v2 = ", v2)

	var v1_plus_v2 = Sum(&v1, &v2)
	fmt.Println("Vector v1 + v2 = ", v1_plus_v2)

	var length_of_v1 float64 = v1.abs()
	fmt.Println("Length of v1 = ", length_of_v1)

	var inverse_of_v1 Vector3D = v1.copy()
	inverse_of_v1.invert()
	fmt.Println("Inverted vector v1 = ", inverse_of_v1)

	// Let's check if v1 + v1' = 0
	var v1_plus_inverse_v1 Vector3D = Sum(&v1, &inverse_of_v1)
	if v1_plus_inverse_v1.abs() < 0.00000001 {
		fmt.Println("v1 + v1' = 0")
	} else {
		fmt.Println("v1 + v1' != 0")
	}

	// Let's scale v2 with the factor 3.0
	var v2_scaled_by_3 Vector3D = v2.copy()
	v2_scaled_by_3.scale(3)
	// An now let us compare the length of v2 and the length of v2_scaled_by_3
	var v2_length float64 = v2.abs()
	var v2_scaled_by_3_length float64 = v2_scaled_by_3.abs()
	// We suddenly see ... the vector scaled by 3.0 has also a length that is 3.0 times bigger
	fmt.Println("v2 length : ", v2_length)
	fmt.Println("v2 scaled with 3 length : ", v2_scaled_by_3_length)
	fmt.Println(v2_length, " * 3.0 = ", (v2_length * 3))

}
